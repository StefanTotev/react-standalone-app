import React from 'react';
import ReactDOM from 'react-dom';

import OwnersDetails from './widgets/OwnersDetails/OwnersDetails';
import PropertyDetails from './widgets/PropertyDetails/PropertyDetails';


ReactDOM.render(<OwnersDetails />, document.querySelector('#owners_details'));
ReactDOM.render(<PropertyDetails />, document.querySelector('#property_details'));