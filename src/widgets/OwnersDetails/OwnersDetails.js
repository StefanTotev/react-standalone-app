import React, { Component } from 'react';

import classes from './OwnersDetails.css';

class OwnersDetails extends Component
{
    render() {
        return (
            <div className={classes.OwnersDetails}>
                <h2>This is <strong>Owners Details</strong> widget</h2>
            </div>
        );
    }
}

export default OwnersDetails;
