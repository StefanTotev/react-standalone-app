import React, { Component } from 'react';

import classes from './PropertyDetails.css';

class PropertyDetails extends Component
{
    render() {
        return (
            <div className={classes.PropertyDetails}>
                <h2>This is <strong>Property Details</strong> widget</h2>
            </div>
        );
    }
}

export default PropertyDetails;
