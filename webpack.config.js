const path = require('path');
const autoprefixer = require('autoprefixer')

module.exports = {
    mode: 'development',

    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
        path: path.resolve(__dirname, 'public/build'),
        filename: "app.bundle.js",
        publicPath: '/build/'
    },

    devtool: 'cheap-module-eval-source-map',
    devServer: {
        contentBase: './public',
        publicPath: '/build/'
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env', "@babel/preset-react"]
                    }
                }
            },

            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: {
                                mode: 'local',
                                localIdentName: '[name]__[local]__[hash:base64:5]'
                            },
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: () => [
                                autoprefixer()
                            ]
                        }
                    }
                ]
            },

            {
                test: /\.(png|jpe?g|gif)$/,
                use: 'url-loader?limit=8000&name=../images/[name].[ext]'
            }
        ]
    }
};